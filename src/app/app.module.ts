import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

// Module Declarations
import { AppRoutingModule } from './app-routing.module';
import { AboutUsModule } from './about-us/about-us.module';
import { CoreModule } from './core/core.module';
import { MinistriesModule } from './ministries/ministries.module';
import { ResourcesModule } from './resources/resources.module';

// Component Declarations
import { AppComponent } from './app.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomepageComponent } from './homepage/homepage.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactUsComponent,
    HomepageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    AboutUsModule,
    MinistriesModule,
    ResourcesModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
