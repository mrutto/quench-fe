export const configuration = {
	blog: {
		posts: [{
			id: 1,
			title: ' First Article',
			author: 'Ann', image: 'B1.jpg',
			publishdate: '2019-02-19T07:22Z',
			excert: 'This is the summary of the article'
		},
		{
			id: 2,
			title: 'Second Article',
			author: 'Berna',
			image: 'b2.jpg',
			publishdate: '2019-02-19T07:22Z',
			excert: 'This is the summary of the article'
		},
		{
			id: 3,
			title: 'Third Article',
			author: 'George',
			image: 'b3.jpg',
			publishdate: '2019-02-19T07:22Z',
			excert: 'This is the summary of the article'
		},
		{
			id: 4,
			title: '4th Article',
			author: 'Frazier',
			image: 'b4.jpg',
			publishdate: '2019-02-19T07:22Z',
			excert: 'This is the summary of the article'
		},
		{
			id: 5,
			title: '5th Article',
			author: 'Harriet',
			image: 'b5.jpg',
			publishdate: '2019-02-19T07:22Z',
			excert: 'This is the summary of the article'
		},
		{
			id: 6,
			title: '6th Article',
			author: 'Esther',
			image: 'b6.jpg',
			publishdate: '2019-02-19T07:22Z',
			excert: 'This is the summary of the article'
		}
		]
	},
};
