import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../services/config.service';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  articleDesctirption: any = [];
  articleData: any = [];
  articleSpecifications: any = [];
  verseCount: Boolean = false;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.http.get('assets/articles/' + id + '.json')
      .subscribe((response) => {
        const test = JSON.stringify(response);
        const desc = JSON.parse(test);
        this.articleDesctirption = desc;
        const dataString = JSON.stringify(desc.data);
        const dataJSON = JSON.parse(dataString);
        this.articleData = dataJSON;
        const articleSpecs = JSON.stringify(desc.specifications);
        const articleJSON = JSON.parse(articleSpecs);
        this.articleSpecifications = articleJSON;
        console.log(this.articleDesctirption.verseCount);
        if (Number(this.articleDesctirption.verseCount) > 0) {
          this.verseCount = true;
        }
      });
  }

  getBack() {
    this.location.back();
  }
}
