import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Component Declarations

import { ArticleComponent } from './article/article.component';
import { BlogComponent } from './blog/blog.component';
import { DownloadsComponent } from './downloads/downloads.component';
import { NavigationComponent } from './navigation/navigation.component';
import { Page1Component } from './page1/page1.component';
import { PastorsHeartComponent } from './pastors-heart/pastors-heart.component';
import { PostComponent } from './post/post.component';
import { ResourceComponent } from './resource/resource.component';

const routes: Routes = [
  {
    path: 'pastors-heart',
    component: PastorsHeartComponent
  },
  {
    path: 'resource',
    children: [
      {
        path: 'downloads',
        component: DownloadsComponent
      },
      {
        path: 'post',
        component: PostComponent
      },
      {
        path: 'article/:id',
        component: ArticleComponent
      },
      {
        path: 'page1',
        component: Page1Component
      },
      {
        path: '',
        component: ResourceComponent
      }
    ]
  },
  {
    path: 'blog',
    component: BlogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourcesRoutingModule { }
