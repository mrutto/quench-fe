import { Component, OnInit } from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.css']
})
export class ResourceComponent implements OnInit {
  
  videoUri: "https://www.googleapis.com/youtube/v3/videos?part=player&id=";
  found:Boolean = false; 
  result: 6;
  obser$;
  public youtubeResults = [];

  nextPageToken: any = [];
  ytvideolist: any = [];

constructor(private sanitizer: DomSanitizer, private http: HttpClient) 
{ 
  
  }

   ngOnInit(){
   
   
  const finalURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=6&playlistId=PLt8ycqgwynvJ4R560ehdH-0oDoJc0U85-&key=AIzaSyDg4k8vxQKcpkxtaH1iH2xhHONARckP5QI";

  console.log('niko hapa 1: ' + finalURL);

 this.obser$ = this.http.get(finalURL)
 .subscribe(data=>{
  const ytString = JSON.stringify(data);
  const ytResults= JSON.parse(ytString);
  console.log(ytResults);
  console.log('niko hapa 2: ' +ytResults.nextPageToken);
  this.youtubeResults = ytResults.items;
  console.table(this.youtubeResults);
  this.found = true; 
  ytResults.items.forEach(obj => {
 
           console.log('niko hapa 3: ' +obj.id.videoId);
           this.ytvideolist.push(obj.snippet.resourceId.videoId);
     
        });
    console.log('niko hapa 4: ' +this.ytvideolist);  
   this.nextPageToken=ytResults.nextPageToken;
 

  }
 );

}
getNextVideos(){
   
  //let relatedToVideoId = this.ytvideolist[2]; 
  const nextUrl="https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&pageToken="+this.nextPageToken+"&maxResults=6&playlistId=PLt8ycqgwynvJ4R560ehdH-0oDoJc0U85-&key=AIzaSyDg4k8vxQKcpkxtaH1iH2xhHONARckP5QI";
  
  
  console.log(nextUrl);
  this.obser$ = this.http.get(nextUrl).subscribe(response=>{
     
      console.log('niko hapa 5: ' +response);
     const ytString = JSON.stringify(response);
      const ytResults= JSON.parse(ytString);
     
      console.log(ytResults.nextPageToken);
      this.nextPageToken = ytResults.nextPageToken;
      // this.ytvideolist=[];
       ytResults.items.forEach(obj => {
          this.youtubeResults.push(obj);

          this.ytvideolist.push(obj.snippet.resourceId.videoId);        
        });
 
      console.log('niko hapa 6: ' +this.ytvideolist); 
      });       

}
  
}
