import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ResourcesRoutingModule } from './resources-routing.module';

// Component Decalration
import { ArticleComponent } from './article/article.component';
import { BlogComponent } from './blog/blog.component';
import { DownloadsComponent } from './downloads/downloads.component';
import { NavigationComponent } from './navigation/navigation.component';
import { Page1Component } from './page1/page1.component';
import { PastorsHeartComponent } from './pastors-heart/pastors-heart.component';
import { PostComponent } from './post/post.component';
import { ResourceComponent } from './resource/resource.component';

@NgModule({
  declarations: [
    ArticleComponent,
    BlogComponent,
    DownloadsComponent,
    NavigationComponent,
    Page1Component,
    PastorsHeartComponent,
    PostComponent,
    ResourceComponent
  ],
  imports: [
    CommonModule,
    ResourcesRoutingModule,
    HttpClientModule
  ]
})
export class ResourcesModule { }
