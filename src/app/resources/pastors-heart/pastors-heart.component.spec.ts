import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastorsHeartComponent } from './pastors-heart.component';

describe('PastorsHeartComponent', () => {
  let component: PastorsHeartComponent;
  let fixture: ComponentFixture<PastorsHeartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastorsHeartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastorsHeartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
