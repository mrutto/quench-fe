import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutUsRoutingModule } from './about-us-routing.module';

// Component Declarations
import { AboutUsComponent } from './common/header/about-us.component';
import { AboutUsHomeComponent } from './home/about-us-home.component';
import { HistoryComponent } from './history/history.component';
import { ServicesComponent } from './services/services.component';
import { TeamComponent } from './team/team.component';

@NgModule({
  declarations: [
    AboutUsComponent,
    AboutUsHomeComponent,
    HistoryComponent,
    ServicesComponent,
    TeamComponent
  ],
  imports: [
    CommonModule,
    AboutUsRoutingModule
  ]
})
export class AboutUsModule { }
