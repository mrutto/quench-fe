import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutUsHomeComponent } from './home/about-us-home.component';
import { HistoryComponent } from './history/history.component';
import { ServicesComponent } from './services/services.component';
import { TeamComponent } from './team/team.component';

const routes: Routes = [
  {
    path: 'about-us',
    children: [
      {
        path: 'home',
        component: AboutUsHomeComponent
      },
      {
        path: 'services',
        component: ServicesComponent
      },
      {
        path: 'history',
        component: HistoryComponent
      },
      {
        path: 'team',
        component: TeamComponent
      },
      {
        path: '',
        redirectTo: '/about-us/home',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsRoutingModule { }
