import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';


@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
	contactForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

  	this.contactForm = this.fb.group({
  	name: ['', [Validators.required, Validators.minLength(4)]],
  	email: ['', [Validators.required, Validators.email]],
  	message: ['', [Validators.required]],
  	})


  }

	get name() { return this.contactForm.get('name'); }
	get email() { return this.contactForm.get('email'); }
	get message() { return this.contactForm.get('message'); }

  onSubmit() {
  // TODO: Use EventEmitter with form value
  //console.warn(this.contactForm.value);

  // stop here if form is invalid
        if (this.contactForm.invalid) {
        	alert('PROCESS FAILED!! :-)' )
            return;
        }

        //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.contactForm.value))

        alert('SUCCESS!! :-)' )
	}

}
