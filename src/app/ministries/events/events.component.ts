import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MinistryService } from '../services/ministry.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  private type;
  public ministries = [];
  constructor(
    private router: Router,
    private ministryService: MinistryService
  ) { }

  ngOnInit() {
    this.type = this.router.url.split('/', 3);
    this.ministryService.getMinistryHome(this.type[2])
      .subscribe(res => {
        this.ministries.push(res);
      });
  }
}
