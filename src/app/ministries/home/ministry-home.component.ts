import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MinistryService } from '../services/ministry.service';

@Component({
  selector: 'app-ministry-home',
  templateUrl: './ministry-home.component.html',
  styleUrls: ['./ministry-home.component.css']
})
export class MinistryHomeComponent implements OnInit {
  
  private type;
  public ministry = [];
  constructor(
    private router: Router,
    private ministryService: MinistryService
  ) { }

  ngOnInit() {
    this.type = this.router.url.split('/', 3);
    const ministry = this.ministryService.getMinistryHome(this.type[2]);
  }

}
