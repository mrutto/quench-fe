import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MinistryService } from '../services/ministry.service';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {

  private type;
  public departments = [];
  constructor(
    private router: Router,
    private ministryService: MinistryService
  ) { }

  ngOnInit() {
    this.type = this.router.url.split('/', 3);
    this.ministryService.getMinistryHome(this.type[2])
      .subscribe(res => {
        for (let i = 1; i <= res.depertments.count; i++) {
          this.departments.push(res.depertments[i]);
        }
      });
  }

}
