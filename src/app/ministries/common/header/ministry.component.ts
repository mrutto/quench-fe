import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MinistryService } from '../../services/ministry.service';

@Component({
  selector: 'app-ministry-header',
  templateUrl: './ministry.component.html',
  styleUrls: ['./ministry.component.css']
})
export class MinistryHeaderComponent implements OnInit {

  public type: string;
  public ministries = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private ministryService: MinistryService
  ) { }

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.ministryService.getMinistryHome(this.type)
      .subscribe(res => {
        this.ministries.push(res);
      });
  }

}
