import { Injectable } from '@angular/core';
import { ApiService } from '../../core/http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MinistryService {

  constructor(
    private api: ApiService
  ) { }

  public getMinistryHome(id: string): Observable<any> {
    return this.api.get('assets/ministries/' + id + '.json')
      .pipe(map(res => {
        const str = JSON.stringify(res);
        const min = JSON.parse(str);
        return min;
      }));
  }
}
