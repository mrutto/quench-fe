import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { AllMinistriesComponent } from './all-ministries/all-ministries.component';
import { DepartmentsComponent } from './departments/departments.component';
import { EventsComponent } from './events/events.component';
import { MinistryComponent } from './ministry.component';
import { MinistryHomeComponent } from './home/ministry-home.component';

const routes: Routes = [
  {
    path: 'ministry',
    children: [
      {
        path: ':type/view',
        component: MinistryComponent,
        children: [
          {
            path: 'home',
            component: MinistryHomeComponent
          },
          {
            path: 'about',
            component: AboutComponent
          },
          {
            path: 'events',
            component: EventsComponent
          },
          {
            path: 'depertments',
            component: DepartmentsComponent
          }
        ]
      },
      {
        path: '',
        component: AllMinistriesComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MinistriesRoutingModule { }
