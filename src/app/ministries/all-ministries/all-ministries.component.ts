import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-ministries',
  templateUrl: './all-ministries.component.html',
  styleUrls: ['./all-ministries.component.css']
})
export class AllMinistriesComponent implements OnInit {

  ministries = [
    {
      name: 'Men\'s ministry',
      path: 'mens',
      ifExist: false
    },
    {
      name: 'Women\'s ministry',
      path: 'womens',
      ifExist: false
    },
    {
      name: 'Youth ministry',
      path: 'youth',
      ifExist: true
    },
    {
      name: 'Teens Ministry',
      path: 'teens',
      ifExist: true
    },
    {
      name: 'Sunday School Ministry',
      path: 'sunday-school',
      ifExist: false
    },
  ];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  navigateToMinistry(ministry) {
    if (!ministry.ifExist) {
      return;
    }
    this.router.navigate(['ministry', ministry.path, 'view', 'about']);
  }

}
