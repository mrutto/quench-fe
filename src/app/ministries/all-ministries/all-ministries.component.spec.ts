import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMinistriesComponent } from './all-ministries.component';

describe('AllMinistriesComponent', () => {
  let component: AllMinistriesComponent;
  let fixture: ComponentFixture<AllMinistriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMinistriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMinistriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
