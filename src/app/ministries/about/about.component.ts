import { Component, OnInit } from '@angular/core';
import { MinistryService } from '../services/ministry.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ministry-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  private type;
  public ministries = [];
  constructor(
    private router: Router,
    private ministryService: MinistryService
  ) { }

  ngOnInit() {
    this.type = this.router.url.split('/', 3);
    this.ministryService.getMinistryHome(this.type[2])
      .subscribe(res => {
        this.ministries.push(res);
      });
  }

}
