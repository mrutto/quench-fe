import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from '../core/core.module';
import { MinistriesRoutingModule } from './ministries-routing.module';

// Component Declarations
import { AboutComponent } from './about/about.component';
import { AllMinistriesComponent } from './all-ministries/all-ministries.component';
import { DepartmentsComponent } from './departments/departments.component';
import { EventsComponent } from './events/events.component';
import { MinistryComponent } from './ministry.component';
import { MinistryHeaderComponent } from './common/header/ministry.component';
import { MinistryHomeComponent } from './home/ministry-home.component';

@NgModule({
  declarations: [
    AboutComponent,
    AllMinistriesComponent,
    DepartmentsComponent,
    EventsComponent,
    MinistryComponent,
    MinistryHeaderComponent,
    MinistryHomeComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    MinistriesRoutingModule
  ]
})
export class MinistriesModule { }
