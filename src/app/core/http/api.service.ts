import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  get(path: string): Observable<any> {
    return this.http.get(`${path}`)
      .pipe(
        catchError(err => {
          return of(err);
        }));
  }

  post(path: string, body): Observable<any> {
    return this.http.post(`${path}`, body)
      .pipe(
        catchError(err => {
          return of(err);
        }));
  }
}
