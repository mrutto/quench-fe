import { Injectable } from '@angular/core';
import { ApiService } from '../http/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(
    private apiService: ApiService
  ) { }

  public sendMail(contact): Observable<any> {
    return this.apiService.post('', {
      email: contact.email,
      name: contact.name,
      emailBody: contact.message
    })
      .pipe(map(
        res => {
          const stringy = JSON.stringify(res);
          const parsed = JSON.parse(stringy);
          return parsed;
        })
      );
  }
}

// this.apiService.post('/users/login', { email: credentials.emailAddress, password: credentials.password })
