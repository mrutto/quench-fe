# Milimani Website DEvelopment ReadMe
# Outline
    1. Introduction
    2. Code Structure

# Introduction
The system is designed as a front end of AIC Milimani's Website.
Designed in Angular 7 and the backend implemented in Laravel.

# Code Structure
All items are marked into:
    [M]  - Modules
    [C]  - Components
    [S]  - Services
    [Mo] - Models
    [D]  - Directories
All components apart from homepage, contact-us and Login should be within its respective module and given a route, if need be within the module's routing module. 
All services that are not shared with other modules should be added as a services within the module. 

*app
    * about-us [M]
        * common [C]
        * history [C]
        * home [C]
        * services [S]
        * team [C]
        - about-us-routing.modue.ts
        - about-us.module.ts
    * contact-us [C]
    * core [M]
        * footer [C]
        * header [C]
        * http [S]
        * services [C]
        - core.module.ts
    * homepage [C]
    * ministries [M]
        * about [C]
        * all-ministries [C]
        * common [C]
        * departments [C]
        * events [C]
        * home  [C]
        * services [S]
        - ministries-routing.module.ts
        - ministries.module.ts
    * resources [M]
        * article  [C]
        * blog [C]
        * downloads [C]
        * navigation [C]
        * page1 [C]
        * pastor's heart [C]
        * post [C]
        * resources [C]
        * services [S]
        - resources-routing.modules.ts
        - resources.moduels.ts
    * shared [M]
        * partials [D]
        -shared.modules.ts
    - app-routing
    - app-module


# Comments
When backing up folders please ensure that the older files are removed from the directory and keeping the directories as clean as possible.
All unsused directories, files and/or services should not be commmited into the repositry and promptly removed. 
All changes should be done on a new branch and the branch should only be merged once the changes have been completed. 
Before merging a branch, ensure that you have pulled into your branch the last committed version of the master branch.

